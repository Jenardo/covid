//
//  ContentView.swift
//  covID
//
//  Created by Gennaro Rivetti on 31/03/2020.
//  Copyright © 2020 Gennaro Rivetti. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
